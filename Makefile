CC=gcc

CFLAGS=-c -Wall

all: lsearch

lsearch: main.o lsearch.o
	$(CC) main.o lsearch.o -o lsearch

main.o: main.c
	$(CC) $(CFLAGS) main.c

lsearch.o: lsearch.c
	$(CC) $(CFLAGS) lsearch.c

clean:
	rm -rf *.o lsearch
