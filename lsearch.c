#include "lsearch.h"
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * �������� �����
 * ���������
 * const void* arr - ������ � ������� ���������� ��������� �����
 * const size_t arr_sz - ������ �������
 * const void* val - �������� �� �������� ���������� ������� �����
 * const size_t val_sz - ������ ��������
 * int(*cmp)(void*, void*) - Callback ��� ��������� ���� ����������
 */

int linear_search(const void* arr, const size_t arr_sz, const void* val, const size_t val_sz, int(*cmp)(const void*, const void*)) {
    for(size_t i = 0; i < arr_sz; ++i) {
        void* index_addr = (void*)arr + i * val_sz;
        if(cmp((void*)val, index_addr))
            return i;
    }
    return -1;
}

int cmp_int(const void* first_val, const void* sec_val) {
    if(!memcmp((int*)first_val, (int*)sec_val, sizeof(int))) {
        return 1;
    }
    return 0;
}

int cmp_char(const void* first_val, const void* sec_val) {
    if(!memcmp((char*)first_val, (char*)sec_val, sizeof(char))) {
        return 1;
    }
    return 0;
}

int cmp_float(const void* first_val, const void* sec_val) {
    if(!memcmp((float*)first_val, (float*)sec_val, sizeof(float))) {
        return 1;
    }
    return 0;
}
