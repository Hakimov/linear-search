#ifndef LSEARCH_H
#define LSEARCH_H

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

int linear_search(const void* arr, const size_t arr_sz, const void* val, const size_t val_sz, int(*cmp)(const void*, const void*));
int cmp_int(const void* first_val, const void* sec_val);
int cmp_char(const void* first_val, const void* sec_val);
int cmp_float(const void* first_val, const void* sec_val);

#endif // LSEARCH_H
