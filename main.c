#include <stdio.h>
#include "lsearch.h"

void int_test()
{
    printf("Int test\n");
    const size_t arr_sz = 10;
    int arr[10];
    for(size_t i = 0; i < arr_sz; ++i) {
        arr[i] = rand() % 10;
        printf("%i i:%i\n", arr[i], i);
    }

    int val = 9;
    int result = linear_search(arr, arr_sz, &val, sizeof(int), cmp_int);
    printf("result index: %i\n\n", result);
}

void char_test()
{
    printf("Char test\n");
    const size_t arr_sz = 10;
    char *arr = "Hello text";
    printf("%s\n", arr);

    char val = 't';
    int result = linear_search(arr, arr_sz, &val, sizeof(char), cmp_char);
    printf("result index: %i\n\n", result);
}

void float_test()
{
    printf("Float test\n");
    const size_t arr_sz = 10;
    float arr[10];
    for(size_t i = 0; i < arr_sz; ++i) {
        arr[i] = rand() % 10;
        printf("%.2f i:%i\n", arr[i], i);
    }
    float val = 7;
    int result = linear_search(arr, arr_sz, &val, sizeof(float), cmp_float);
    printf("result index: %i\n\n", result);
}

void test()
{
    int_test();
    char_test();
    float_test();
}

int main()
{
    test();
    return 0;
}
